package main

import (
	"errors"
	"log"
	"net/http"
	"regexp"
)

func route(w http.ResponseWriter, req *http.Request) {
	// regex, _ := regexp.Compile("^/([a-zA-Z0-9]*)/{0,1}([0-9]*)$")
	// matches := regex.FindStringSubmatch(req.URL.Path)

	// if len(matches) < 2 {
	// 	w.WriteHeader(http.StatusNotFound)
	// 	w.Write([]byte("404 - Not found."))
	// 	return
	// }

	// pattern := "^/([a-zA-Z0-9]*)/{0,1}([0-9]*)$"
	// matches, err := validateRoute(pattern, req.URL.Path)
	// if err != nil {
	// 	w.WriteHeader(http.StatusNotFound)
	// 	w.Write([]byte("404 - Not found."))
	// 	return
	// }

	switch req.Method {
	case http.MethodPost:
		pattern := "^/([a-zA-Z0-9]*)/{0,1}([0-9]*)$"
		matches, err := validateRoute(pattern, req.URL.Path)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - Not found."))
			return
		}
		post(w, req, matches)
		break
	case http.MethodGet:
		pattern := "^/([a-zA-Z0-9]*)/{0,1}([0-9]*)$"
		matches, err := validateRoute(pattern, req.URL.Path)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - Not found."))
			return
		}
		get(w, req, matches)
		break
	case http.MethodDelete:
		pattern := "^/([a-zA-Z0-9]*)/{0,1}([0-9a-zA-Z]*)$"
		matches, err := validateRoute(pattern, req.URL.Path)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - Not found."))
			return
		}
		delete(w, req, matches)
	default:
		log.Print("405 - Method not allowed. Only POST and DELETE methods are allowed.")
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Method not allowed. Only POST and DELETE methods are allowed."))
	}
}

func validateRoute(pattern, path string) ([]string, error) {	
	regex, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}

	matches := regex.FindStringSubmatch(path)
	if len(matches) < 2 {		
		return nil, errors.New("Wrong numer of argments.")
	}

	return matches, nil
}
