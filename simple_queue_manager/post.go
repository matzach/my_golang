package main

import (
	"crypto/sha256"
	"encoding/hex"
	List "hello/learning_projects/simple_queue_manager/list"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

func post(w http.ResponseWriter, req *http.Request, matches []string) {
	m, err := ioutil.ReadAll(req.Body)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad request."))
		return
	}

	body := string(m)

	if len(body) > maxMessageSize {
		log.Print("400 - Bad request. Message to long. Max 256B.")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad request. Message to long. Max 256B."))
		return
	}
	queueName := matches[1]
	visibilityTime, err := strconv.Atoi(matches[2])
	if err != nil {
		log.Print("Wrong format of visibility time value.")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad request. Wrong format of visibility time value."))
		return
	}

	PostMessage(queueName, body, visibilityTime)
}

func PostMessage(queueName, body string, vt int) {
	q := queues[queueName]

	if q.Empty() {
		queues[queueName] = List.New(*Message(body, vt))
	} else {
		var e List.Element
		e.Value = *Message(body, vt)
		q.Add(e)
		queues[queueName] = q
	}
}

func Message(body string, vt int) *List.ValueType {
	m := new(List.ValueType)

	m.ID = genID(body)
	m.Body = body
	m.VisibilityTime = vt

	return m
}

func genID(s string) string {	
	b := []byte(s)
	t := strconv.FormatInt(time.Now().UnixNano(), 10)

	id := sha256.Sum256(append(b, []byte(t)...))

	return hex.EncodeToString(id[:])
}
