package main

import (	
	"net/http"
	"time"
)

func delete(w http.ResponseWriter, req *http.Request, matches []string) {
	go gDelete(matches)	
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusAccepted)	
}

func gDelete(matches []string) {	
	dc := DelChan{matches[1], matches[2]}	

	var sent bool = false
	for {
		if len(channels) == 0 {
			break
		}
		for _, c := range channels {
			select {
			case c <- dc:				
			default:				
				time.Sleep(time.Duration(200) * time.Millisecond)
			}
		}

		if sent {			
			break
		}
	}
}
