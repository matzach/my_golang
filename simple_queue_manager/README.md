# What?
This is simple queue server written in Go.

# Why?
This is my first project in goland. I've created it only for learning purposes.

# Main assumptions
1. Run server with param `--port=9999` to change port. Default port is 8080
2. Send message by REST API `POST /{queue_name}/{visibility_time}` Payload: as simple string. `visibility_time` is not used now.
3. Get message `GET /{queue_name}`
4. To delete message `DELETE /{queue_name}/{message_id}`
5. It creates new queue if not created yet
6. FIFO
7. Not persistent. Keeps messages in memory.


@TODO Catch exit signal from system
@TODO Use `flag` for CLI args https://gobyexample.com/command-line-flags
@TODO Add memory limit to star storing messages on disc
@TODO DLQ and configuration
@TODO Code message as BASE64 inside system
@TODO Make list pkg as separate package on github
@TODO Make pub sub
@TODO Make persistence
@TODO pub-sub feature
@TODO Consider using mux https://github.com/gorilla/mux
