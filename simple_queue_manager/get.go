package main

import (
	"context"
	"encoding/json"
	List "hello/learning_projects/simple_queue_manager/list"
	"net/http"
	"time"
)

type DelChan struct {
	QueueName string
	ID string
}

var channels []chan DelChan = make([]chan DelChan, 0)

func get(w http.ResponseWriter, req *http.Request, matches []string) {
	queueName := matches[1]
	q := queues[queueName]
	if q.Empty() {
		w.WriteHeader(http.StatusNoContent)
		w.Write([]byte("204 - Queue is empty or dosn't exist."))
		return
	}

	m := q.First().Value
	go hide(m, queueName, addChan())	

	q.Delete()
	queues[queueName] = q

	j, _ := json.Marshal(m)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")	
	w.WriteHeader(http.StatusOK)
	w.Write(j)
	return
}

func hide(m List.ValueType, queueName string, c chan DelChan) {	
	d := time.Now().Add(time.Duration(m.VisibilityTime) * time.Second)
	ctx, cancel := context.WithDeadline(context.Background(), d)
	defer cancel()
	fin := false

	for {
		select {
		case dc := <-c:
			if queueName == dc.QueueName && m.ID == dc.ID {
				removeChan(c)
				return
			}
		case <-ctx.Done():
			fin = true
		}

		if fin {
			removeChan(c)
			break
		}
	}
	
	removeChan(c)
	PostMessage(queueName, m.Body, m.VisibilityTime)	
}

func addChan() chan DelChan{
	c := make(chan DelChan)
	channels = append(channels, c)

	return c
}

func removeChan(c chan DelChan) {	
	for i, val := range channels {
		if val == c {
			channels = append(channels[:i], channels[i+1:]...)			
		}
	}
}
