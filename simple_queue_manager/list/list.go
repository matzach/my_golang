package list

type ValueType struct {
	ID string
	Body string
	VisibilityTime int
}

type Element struct {
	Value ValueType
	next  *Element
}

type List struct {
	first *Element
}

func New(value ValueType) List {
	var list List
	var element Element
	element.Value = value
	list.Add(element)

	return list
}

func (l *List) First() *Element {
	return l.first
}

func (l *List) Next(e *Element) *Element {
	return e.next
}

func (l *List) Add(element Element) {
	if l.Empty() {
		l.first = &element
		return
	}

	last := l.Last()
	last.next = &element
}

func (l *List) Last() *Element {
	return l.Traverse(l.first, func(e *Element) {})
}

func (l *List) Traverse(e *Element, f func(*Element)) *Element {
	f(e)
	if l.Empty() {
		return e
	}
	if e == nil || e.next == nil {
		return e
	}

	return l.Traverse(e.next, f)
}

func (l *List) Delete() {
	if l.Empty() {
		return
	}
	l.first = l.first.next
}

func (l *List) MoveBack() {
	if l.Empty() {
		return
	}

	first := *l.first
	first.next = nil
	l.Add(first)
	l.Delete()
}

func (l *List) Empty() bool {
	if l == nil {
		return true
	}
	if l.first == nil {
		return true
	}
	return false
}
