package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

func getPort() string {
	var b strings.Builder
	args := os.Args

	regex, _ := regexp.Compile("^--port=([0-9]{1,6})")

	for _, a := range args {
		matches := regex.FindStringSubmatch(a)
		if len(matches) > 1 {
			fmt.Fprintf(&b, ":%s", matches[1])
			return b.String()
		}

	}

	return defaultPort
}
