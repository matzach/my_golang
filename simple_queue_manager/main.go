package main

import (	
	"fmt"
	List "hello/learning_projects/simple_queue_manager/list"	
	"log"
	"net/http"
	"os"
)

type Queue = List.List

const (
	defaultPort = ":8080"	
	maxMessageSize = 256 //bytes
)

var queues map[string]Queue = make(map[string]Queue)
var f *os.File

func init() {
	f, _ := os.OpenFile("testlogfile", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	log.SetOutput(f)
}

func main() {
	
	port := getPort()
	fmt.Println("Server start")
	fmt.Printf("Listening at port: %s\n", port)
	defer fmt.Println("Server stop")

	http.HandleFunc("/", route)

	if err := http.ListenAndServe(port, nil); err != nil {
		log.Fatal(err)
	}
}
