package jwt

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"log"
	"net/http"
	"os"
	"strings"

	"github.com/cristalhq/jwt/v3"
)

var secret string
var ErrUserNotAuthorized = errors.New("user not authorized")

type jwtPayload struct {
	ID string
}

var userCtxKey = &ContextKey{"user_ID"}

type ContextKey struct {
	name string
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		secret = os.Getenv("SNAILCOM_SECRET")
		log.Println("secret", secret)

		authHeader := r.Header.Get("Authorization")
		tokenSplit := strings.Split(authHeader, " ")
		if len(tokenSplit) != 2 {
			http.Error(w, "Invalid token", http.StatusForbidden)

			return
		}

		userID, err := verify(tokenSplit[1])		
		if err != nil {	
			http.Error(w, fmt.Errorf("%w", err).Error(), http.StatusForbidden)

			return
		}

		ctx := context.WithValue(r.Context(), userCtxKey, userID)
		r = r.WithContext(ctx)
		
		next.ServeHTTP(w, r)		
	})
}

func verify(tokenStr string) (string, error) {
	var userID string
	key := []byte(secret)
	verifier, err := jwt.NewVerifierHS(jwt.HS256, key)
	if err != nil {
		return userID, fmt.Errorf("%w", err)
	}
	
	token, err := jwt.ParseAndVerifyString(tokenStr, verifier)
	if err != nil {
		return userID, fmt.Errorf("%w", err)
	}

	var claims jwtPayload
	if err := json.Unmarshal(token.RawClaims(), &claims); err != nil {
		return userID, fmt.Errorf("%w", err)
	}

	return string(claims.ID), nil
}

func Authorize(userID, action, entity string) (bool, error) {
	client := &http.Client{}

	domain := os.Getenv("SNAILCOM_DOMAIN")
	url := fmt.Sprintf("%s/api/authorize/%s/%s/%s", domain, userID, action, entity)

	fmt.Println(url)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return false, fmt.Errorf("%w", err)
	}

	req.Header = map[string][]string{
		"api-x-key": {"SNAILBOOK_X_KEY"},
	}

	resp, err := client.Do(req)
	if err != nil {
		return false, fmt.Errorf("%w", err)
	}

	fmt.Println("STATUS", resp.StatusCode)

	return resp.StatusCode == http.StatusOK, nil
}

func ForContext(ctx context.Context) string {
	raw, _ := ctx.Value(userCtxKey).(string)
	return raw
}
