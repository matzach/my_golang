ALTER TABLE roots ADD COLUMN creation_time INT;
ALTER TABLE threads ADD COLUMN creation_time INT;
ALTER TABLE comments ADD COLUMN creation_time INT;