package database

import (
	"database/sql"
	"errors"
	"log"

	// This redeclares package bellow.
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
)

var Db *sql.DB

func InitDB() {
	db, err := sql.Open("mysql", "root:root@tcp(localhost)/snailbook_comments")
	if err != nil {
		log.Panic(err)
	}

	if err = db.Ping(); err != nil {
		log.Panic(err)
	}

	Db = db
}

func Migrate() {
	if err := Db.Ping(); err != nil {
		log.Fatal(err)
	}

	driver, _ := mysql.WithInstance(Db, &mysql.Config{})
	m, _ := migrate.NewWithDatabaseInstance(
		"file://../internal/pkg/db/migrations/mysql",
		"mysql",
		driver,
	)

	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		log.Fatal(err)
	}
}
