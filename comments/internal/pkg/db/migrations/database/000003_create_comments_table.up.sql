CREATE TABLE IF NOT EXISTS comments (
    id INT NOT NULL UNIQUE AUTO_INCREMENT,
    author VARCHAR (127),
    thread_id INT NULL,    
    body VARCHAR (2000),
    FOREIGN KEY (thread_id) REFERENCES threads(id),
    PRIMARY KEY (id)
)
