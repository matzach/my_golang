package comments

import (
	"errors"
	"fmt"
	"snailcom2/internal/pkg/db/migrations/database"
	"snailcom2/internal/statuses"
	"time"
)

type Comment struct {
	ID           string
	Author       string
	Body         string
	ThreadID     string
	CreationTime int64
}

var errThreadNotExists = errors.New("thread not exists")

func (comment *Comment) Save() (int64, error) {
	if !comment.threadExists() {
		return 0, fmt.Errorf("%w", errThreadNotExists)
	}

	sql := "INSERT INTO comments (thread_id, author, body, creation_time, status) VALUES (?,?,?,?,?)"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	comment.CreationTime = time.Now().Unix()

	res, err := stmt.Exec(comment.ThreadID, comment.Author, comment.Body, comment.CreationTime, statuses.PUBLISHED)
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}

	return id, nil
}

func (comment *Comment) threadExists() bool {
	sql := "SELECT id FROM threads WHERE id = ? AND status = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return false
	}
	defer stmt.Close()

	rows, err := stmt.Query(comment.ThreadID, statuses.PUBLISHED)
	if err != nil {
		return false
	}
	defer rows.Close()

	return rows.Next()
}

func GetComments(threadID string) ([]*Comment, error) {
	var comments []*Comment

	sql := "SELECT id, author, body, COALESCE(creation_time, 0) FROM comments WHERE thread_id = ? AND status = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return comments, fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(threadID, statuses.PUBLISHED)
	if err != nil {
		return comments, fmt.Errorf("%w", err)
	}
	defer rows.Close()

	for rows.Next() {
		var comment Comment
		err = rows.Scan(
			&comment.ID,
			&comment.Author,
			&comment.Body,
			&comment.CreationTime,
		)

		if err != nil {
			return comments, fmt.Errorf("%w", err)
		}

		comments = append(comments, &comment)
	}

	return comments, nil
}

func DeleteInThread(threadID string) error {
	sql := "SELECT id FROM comments WHERE thread_id = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(threadID)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer rows.Close()

	for rows.Next() {
		var comment Comment

		if err := rows.Scan(&comment.ID); err != nil {
			return fmt.Errorf("%w", err)
		}

		if err := Delete(comment.ID); err != nil {
			return fmt.Errorf("%w", err)
		}
	}

	return nil
}

func Delete(commentID string) error {	
	sql := "UPDATE comments SET status = ? WHERE id = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(statuses.DELETED, commentID)
	if err != nil {
		return fmt.Errorf("%w", err)
	}

	return nil
}
