package roots

import (
	"context"
	"errors"
	"fmt"
	"snailcom2/internal/actions"
	"snailcom2/internal/pkg/db/migrations/database"
	"snailcom2/internal/statuses"
	"snailcom2/internal/threads"
	"snailcom2/jwt"
	"time"
)

type Root struct {
	ID           string
	Entity       string
	CreationTime int64
	Threads      *[]threads.Thread
}

var errRootNotExists = errors.New("root not exists")

func (root *Root) Save(ctx context.Context) (int64, error) {	
	auth, err := jwt.Authorize(jwt.ForContext(ctx), root.Entity, actions.CREATE_ROOT)
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	} else if !auth {
		return 0, jwt.ErrUserNotAuthorized
	}

	sql := "INSERT INTO roots (entity, creation_time, status) VALUES (?, ?, ?)"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	root.CreationTime = time.Now().Unix()

	res, err := stmt.Exec(root.Entity, root.CreationTime, statuses.PUBLISHED)
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}

	return id, nil
}

func Find(entity string, ctx context.Context) (Root, error) {
	var root Root

	sql := "SELECT id, entity, creation_time FROM roots WHERE entity = ? AND status = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return root, fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(entity, statuses.PUBLISHED)
	if err != nil {
		return root, fmt.Errorf("%w", err)
	}
	defer rows.Close()

	if !rows.Next() {
		return root, fmt.Errorf("%w", errRootNotExists)
	}

	err = rows.Scan(&root.ID, &root.Entity, &root.CreationTime)
	if err != nil {
		return root, fmt.Errorf("%w", err)
	}

	auth, err := jwt.Authorize(jwt.ForContext(ctx), root.Entity, actions.QUERY_ROOT)
	if err != nil {
		return root, fmt.Errorf("%w", err)
	} else if !auth {
		return root, jwt.ErrUserNotAuthorized
	}

	if err := root.getThreads(); err != nil {
		return root, fmt.Errorf("%w", err)
	}

	return root, nil
}

func (root *Root) getThreads() error {
	var ts []threads.Thread

	sql := "SELECT id, author, body, COALESCE(creation_time, 0) FROM threads WHERE root_id = ? AND status = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(root.ID, statuses.PUBLISHED)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer rows.Close()

	for rows.Next() {
		var thread threads.Thread
		err := rows.Scan(
			&thread.ID,
			&thread.Author,
			&thread.Body,
			&thread.CreationTime,
		)
		if err != nil {
			return fmt.Errorf("%w", err)
		}

		ts = append(ts, thread)
	}

	root.Threads = &ts

	return nil
}

func Delete(rootID string, ctx context.Context) error {
	root, err := Find(rootID, ctx)
	if err != nil {
		return fmt.Errorf("%w", err)
	}

	auth, err := jwt.Authorize(jwt.ForContext(ctx), root.Entity, actions.QUERY_ROOT)
	if err != nil {
		return fmt.Errorf("%w", err)
	} else if !auth {
		return jwt.ErrUserNotAuthorized
	}

	sql := "UPDATE roots SET status = ? WHERE id = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(statuses.DELETED, rootID)
	if err != nil {
		return fmt.Errorf("%w", err)
	}

	threads.DeleteInRoot(rootID, ctx)

	return nil
}
