package threads

import (
	"context"
	"errors"
	"fmt"
	"snailcom2/internal/actions"
	"snailcom2/internal/comments"
	"snailcom2/internal/pkg/db/migrations/database"
	"snailcom2/internal/statuses"
	"snailcom2/jwt"
	"time"
)

type Thread struct {
	ID           string
	RootID       string
	Author       string
	Body         string
	CreationTime int64
	Comments     *[]comments.Comment
}

var errRoorNotExists = errors.New("root does not exist")

func (thread *Thread) Save(ctx context.Context) (int64, error) {
	if !rootExists(thread.RootID) {
		roorNotExistsErr := fmt.Errorf("%w", errRoorNotExists)

		return 0, roorNotExistsErr
	}

	auth, err := jwt.Authorize(jwt.ForContext(ctx), getEntity(thread.RootID), actions.CREATE_THREAD)
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	} else if !auth {
		return 0, jwt.ErrUserNotAuthorized
	}

	sql := "INSERT INTO threads (root_id, author, body, creation_time, status) VALUES (?,?,?,?,?)"
	stmt, err := database.Db.Prepare(sql)

	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	thread.CreationTime = time.Now().Unix()
	res, err := stmt.Exec(thread.RootID, thread.Author, thread.Body, thread.CreationTime, statuses.PUBLISHED)

	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("%w", err)
	}

	return id, nil
}

func Delete(threadID string, ctx context.Context) error {
	thread, err := find(threadID)
	if err != nil {
		return fmt.Errorf("%w", err)
	}

	auth, err := jwt.Authorize(jwt.ForContext(ctx), getEntity(thread.RootID), actions.CREATE_THREAD)
	if err != nil {
		return fmt.Errorf("%w", err)
	} else if !auth {
		return jwt.ErrUserNotAuthorized
	}


	sql := "UPDATE threads SET status = ? WHERE id = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(statuses.DELETED, threadID)
	if err != nil {
		return fmt.Errorf("%w", err)
	}

	if err := comments.DeleteInThread(threadID); err != nil {
		return err
	}

	return nil
}

func DeleteInRoot(rootID string, ctx context.Context) error {
	sql := "SELECT id FROM threads WHERE root_id = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(rootID)
	if err != nil {
		return fmt.Errorf("%w", err)
	}
	defer rows.Close()

	for rows.Next() {
		var thread Thread

		if err := rows.Scan(&thread.ID); err != nil {
			return fmt.Errorf("%w", err)
		}

		if err := Delete(thread.ID, ctx); err != nil {
			return fmt.Errorf("%w", err)
		}
	}

	return nil
}

func rootExists(rootID string) bool {
	stmt, err := database.Db.Prepare("SELECT * FROM roots WHERE id = ? AND status = ?")
	if err != nil {
		return false
	}
	defer stmt.Close()

	rows, err := stmt.Query(rootID, statuses.PUBLISHED)
	if err != nil {
		return false
	}
	defer rows.Close()

	return rows.Next()
}

func getEntity(rootID string) string {
	var entity string
	stmt, err := database.Db.Prepare("SELECT entity FROM roots WHERE id = ? AND status = ?")
	if err != nil {
		return entity
	}
	defer stmt.Close()

	rows, err := stmt.Query(rootID, statuses.PUBLISHED)
	if err != nil {
		return entity
	}
	defer rows.Close()

	if rows.Next() {
		err := rows.Scan(&entity)
		if err != nil {
			return entity
		}
	}

	return entity
}

func find(threadID string) (Thread, error) {
	var thread Thread

	sql := "SELECT id, author, body, COALESCE(creation_time, 0) FROM threads WHERE id = ? AND status = ?"
	stmt, err := database.Db.Prepare(sql)
	if err != nil {
		return thread, fmt.Errorf("%w", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(threadID, statuses.PUBLISHED)
	if err != nil {
		return thread, fmt.Errorf("%w", err)
	}
	defer rows.Close()

	if rows.Next() {		
		err := rows.Scan(
			&thread.ID,
			&thread.Author,
			&thread.Body,
			&thread.CreationTime,
		)
		if err != nil {
			return thread, fmt.Errorf("%w", err)
		}
	}

	return thread, nil
}
