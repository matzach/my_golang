package actions

const (
	CREATE_ROOT = "create_root"
	CREATE_THREAD = "create_thread"
	CREATE_COMMENT = "create_comment"
	QUERY_ROOT = "query_root"
	QUERY_THREAD = "query_thread"
	QUERY_COMMENT = "query_comment"
	DELETE_ROOT = "delete_root"
	DELETE_THREAD = "delete_thread"
	DELETE_COMMENT = "delete_comment"
)
