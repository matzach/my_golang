package main

import (
	"log"
	"net/http"
	"os"
	"snailcom2/graph"
	"snailcom2/graph/generated"
	"snailcom2/internal/pkg/db/migrations/database"
	"snailcom2/jwt"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)

const defaultPort = "8081"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	database.InitDB()

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", jwt.AuthMiddleware(srv))

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
