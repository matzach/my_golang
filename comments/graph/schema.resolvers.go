package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"snailcom2/graph/generated"
	"snailcom2/graph/model"
	"snailcom2/internal/comments"
	"snailcom2/internal/roots"
	"snailcom2/internal/threads"	
	"strconv"
)

const (
	DECIMAL = 10
)

func (r *mutationResolver) CreateRoot(ctx context.Context, input model.NewRoot) (*model.Root, error) {
	var root roots.Root
	root.Entity = input.Entity	
	rootID, err := root.Save(ctx)

	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return &model.Root{
		ID:           strconv.FormatInt(rootID, DECIMAL),
		Entity:       root.Entity,
		CreationTime: int(root.CreationTime),
	}, nil
}

func (r *mutationResolver) CreateThread(ctx context.Context, input model.NewThread) (*model.Thread, error) {
	var thread threads.Thread
	thread.RootID = input.RootID
	thread.Author = input.Author
	thread.Body = input.Body
	threadID, err := thread.Save(ctx)

	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return &model.Thread{
		ID:           strconv.FormatInt(threadID, DECIMAL),
		Author:       thread.Author,
		Body:         thread.Body,
		CreationTime: int(thread.CreationTime),
		Comments:     []*model.Comment{},
	}, nil
}

func (r *mutationResolver) CreateComment(ctx context.Context, input model.NewComment) (*model.Comment, error) {
	var comment comments.Comment
	comment.Author = input.Author
	comment.Body = input.Body
	comment.ThreadID = input.ThreadID	
	cID, err := comment.Save()

	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return &model.Comment{
		ID:           strconv.FormatInt(cID, DECIMAL),
		Author:       comment.Author,
		Body:         comment.Body,
		CreationTime: int(comment.CreationTime),
	}, nil
}

func (r *mutationResolver) DeleteRoot(ctx context.Context, id string) (string, error) {
	if err := roots.Delete(id, ctx); err != nil {
		return "error", err
	}

	return "ok", nil
}

func (r *mutationResolver) DeleteThread(ctx context.Context, id string) (string, error) {
	if err := threads.Delete(id, ctx); err != nil {
		return "error", err
	}

	return "ok", nil
}

func (r *mutationResolver) DeleteComment(ctx context.Context, id string) (string, error) {
	if err := comments.Delete(id); err != nil {
		return "error", err
	}

	return "ok", nil
}

func (r *queryResolver) Root(ctx context.Context, entity string) (*model.Root, error) {
	root, err := roots.Find(entity, ctx)

	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	var mts []*model.Thread

	for _, thread := range *root.Threads {
		mt := &model.Thread{
			ID:           thread.ID,
			Author:       thread.Author,
			Body:         thread.Body,
			CreationTime: int(thread.CreationTime),
		}

		mts = append(mts, mt)
	}

	return &model.Root{
		ID:           root.ID,
		Entity:       root.Entity,
		CreationTime: int(root.CreationTime),
		Threads:      mts,
	}, nil
}

func (r *queryResolver) Comments(ctx context.Context, threadID *string) ([]*model.Comment, error) {
	var mcs []*model.Comment

	comments, err := comments.GetComments(*threadID)

	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	for _, comment := range comments {
		mc := &model.Comment{
			ID:           comment.ID,
			Author:       comment.Author,
			Body:         comment.Body,
			CreationTime: int(comment.CreationTime),
		}

		mcs = append(mcs, mc)
	}

	return mcs, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
