# URL SHORTENER
1. Simple url shortener
2. PSQL database
3. Release on heroku
4. Short url to `/<20 letters slug>` [a-zA-Z0-9\-\_]
5. You can use your own slugs (20 signs)
6. You can add only 1 url within 1 second (configurable)
7. Check if slug exists, 
8. Add simple API `POST /api/url` payload: `{url: string, slug: string}` 200 OK, 400 BAD REQUEST
9. Redirect with 301 status code
10. Przekierowanie tylko wtedy gdy zwraca 200 i nie kieruje z powrotem do tej strony
11. Sprawdzanie sql injection - escaping


# DATABASE SCHEMA
```SQL
CREATE TABLE redirection (
    id SERIAL PRIMARY KEY,
    url VARCHAR NOT NULL,
    slug VARCHAR (5) UNIQUE NOT NULL
);
```

CREATE TABLE redirection (id SERIAL PRIMARY KEY,url VARCHAR NOT NULL,slug VARCHAR (5) UNIQUE NOT NULL);

# How to login to database
`psql -Uurl_shortener`