package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5433
	user     = "url_shortener"
	password = "url_shortener"
	dbname   = "url_shortener"
	maxSlugLen = 20
	domain = "url.shortener"
	rangSlugLen = 5
  )

type Redirection struct {
	Url string
	Slug string
}

var db *sql.DB
var err error
var chars = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func main() {
	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname,
	)
	db, err = sql.Open("postgres", psqlInfo)

	if err != nil { panic(err) } else { fmt.Println("DB CONNECTED") }	

	defer db.Close()

	fmt.Println("Server started.")
	r := mux.NewRouter()	
	r.HandleFunc("/api/url", PostRedirection).Methods("POST")
	r.HandleFunc("/{slug}", Redirect).Methods("GET")
	r.Handle("", http.FileServer(http.Dir("./public")))
	http.Handle("/", r)
	
	err := http.ListenAndServe(":8001", nil)

	if err != nil {
		panic(err)
	}
}

func Redirect(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)	

	query := `SELECT url, slug FROM redirection WHERE slug = $1`

	var dr Redirection

	row := db.QueryRow(query, vars["slug"])

	switch err := row.Scan(&dr.Url, &dr.Slug); err {
	case sql.ErrNoRows:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("404 - Redirection not found"))
		return
	case nil:
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("404 - Redirection not found"))
		return
	}
	
	w.Header().Add("Location", dr.Url)
	w.WriteHeader(http.StatusMovedPermanently)
	w.Write([]byte(""))
}

func PostRedirection(w http.ResponseWriter, r *http.Request) {
	m, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad request."))
		return
	}

	var payload Redirection
	if json.Unmarshal(m, &payload) != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad request."))
		return
	}

	err = payload.PayloadValidation()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	err = payload.save()

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	resp, _ := json.Marshal(payload)
	
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp))
}

func (r *Redirection) PayloadValidation() error  {	
	if len(r.Url) == 0 {
		return errors.New("url field is empty")
	}

	var DefaultTransport http.RoundTripper = &http.Transport{}
	req, _ := http.NewRequest("GET", r.Url, nil)
	resp, err := DefaultTransport.RoundTrip(req)

	if err != nil {
		return errors.New("unknow error during GET request to given url")
	}
	
	if resp.StatusCode > 399 {
		return errors.New("GET request to given urls should return 200")
	}
	
	// @TODO Detect rediretion loop
	// https://jonathanmh.com/tracing-preventing-http-redirects-golang/
	
	if len(r.Slug) > 0 && !regexp.MustCompile(`^[A-Za-z0-9\-\_]+$`).MatchString(r.Slug) {		
		return errors.New("slug can contain only letter, numbers, dash and underscore")
	}
	
	
	if len(r.Slug) > maxSlugLen { 
		return fmt.Errorf("slug can be maximaly %d letters long", maxSlugLen)
	}

	if strings.Contains(r.Url, domain) {
		return errors.New("you can't redirect to this app")
	}

	if len(r.Slug) == 0 {
		r.randSlug(rangSlugLen)
	}

	fmt.Println(r)

	return nil
}

func (r *Redirection) randSlug(l int) {
	query := `SELECT slug FROM redirection WHERE slug = $1`

	var dr Redirection
	var err error
	var slug []rune

	for err == nil {
		slug = make([]rune, l)
		rand.Seed(time.Now().UnixNano())
		
		for i := range slug {
			slug[i] = chars[rand.Intn(len(chars))]
		}

		row := db.QueryRow(query, string(slug))
		err = row.Scan(&dr.Slug)
		fmt.Println(string(slug))
	}

	r.Slug = string(slug)
}

func (r *Redirection) save() error {
	sql := `INSERT INTO redirection (url, slug) VALUES ($1, $2)`
	_, err = db.Exec(sql, r.Url, r.Slug)

	if err != nil {		
		return errors.New("slug is already in use")
	}

	return nil
}
